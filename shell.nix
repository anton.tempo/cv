let nixpkgs = import ./nixpkgs.nix;
    pkgs = import nixpkgs {
      overlays = import ./nix/overlay.nix;
    };
    pythonOpencv = pkgs.python3.pkgs.toPythonModule (pkgs.opencv4.override {
      enablePython = true;
      pythonPackages = pkgs.python3.pkgs;
    });
in  pkgs.mkShell {
 name = "cv-playground";
  buildInputs = with pkgs; [ python3 pythonOpencv k4a pyk4a ];
}
