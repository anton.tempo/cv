{ stdenv, dpkg, autoPatchelfHook, k4a, libGL, libX11, udev, libXrandr
, libXinerama, libXcursor, libXxf86vm, libsoundio-1
, version, url, sha256 }:

stdenv.mkDerivation rec {
  name = "k4a-tools";
  inherit version;

  nativeBuildInputs = [ dpkg autoPatchelfHook ];
  buildInputs = [ k4a libGL libX11 udev stdenv.cc.cc.lib libXrandr libXinerama
    libXcursor libXxf86vm libsoundio-1 ];

  src = builtins.fetchurl {
    inherit url sha256;
  };

  unpackPhase = ''
    dpkg-deb -x $src ${name}
    ls ${name}
  '';

  installPhase = ''
    mkdir -p "$out"
    cp -R "${name}/usr/bin" "$out/bin/"
  '';
}
