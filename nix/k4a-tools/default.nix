{ callPackage, version }:

let versions = {
      "1.3.0" = {
        url = "https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/k/k4a-tools/k4a-tools_1.3.0_amd64.deb";
        sha256 = "1mmgwbwjichj1jaf0p54ymdw9c8ra6gyhdlabb2dxv1q5x5a2zmd";
      };
      "1.4.1" = {
        url = "https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/k/k4a-tools/k4a-tools_1.4.1_amd64.deb";
        sha256 = "0zrlcbcviiw4jx34chc34k170gad8hd4kh864a1bq0hs7hvk8l3g";
      };
    };
in  callPackage ./common.nix ({inherit version;} // builtins.getAttr version versions)
