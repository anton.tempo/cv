{ callPackage, udev, stdenv
, version }:

let versions = {
      "1.3.0" = {
        k4a = {
          url = "https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4a1.3/libk4a1.3_1.3.0_amd64.deb";
          sha256 = "16pj9qsnr2wk4y03f0sc2f4zcbili09cyfrir9k93zsap099f64k";
        };
        k4a-dev = {
          url = "https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4a1.3-dev/libk4a1.3-dev_1.3.0_amd64.deb";
          sha256 = "0iy2vz2zqvz3r303n5yncc3qcyq88nxbp1hbr8vklgv2bf91660l";
        };
        patches = [ ./1.3.0-cmakemodules.patch ];
        postFixup = ''
          echo "Adjusting RPATH for libk4a"
          patchelf --set-rpath '${placeholder "out"}/lib:${udev.out}/lib:${stdenv.cc.cc.lib}/lib' "$out/lib/libk4a.so.${version}"

          echo "Adjusting RPATH for libk4arecord"
          patchelf --set-rpath '${placeholder "out"}/lib:${stdenv.cc.cc.lib}/lib' "$out/lib/libk4arecord.so.${version}"

          autoPatchelf "$out/lib/libdepthengine.so.2.0"
        '';
      };
      "1.4.1" = {
        k4a = {
          url = "https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4a1.4/libk4a1.4_1.4.1_amd64.deb";
          sha256 = "0ackdiakllmmvlnhpcmj2miix7i2znjyai3a2ck17v8ycj0kzin1";
        };
        k4a-dev = {
          url = "https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4a1.4-dev/libk4a1.4-dev_1.4.1_amd64.deb";
          sha256 = "1llw52i6bqgm5a7d32rfvmrmw1cp1javij4vq5sfldmdp6a30c08";
        };
        patches = [ ./1.4.1-cmakemodules.patch ];
        postFixup = ''
          echo "Adjusting RPATH for libk4a"
          patchelf --set-rpath '${placeholder "out"}/lib/libk4a1.4:${udev.out}/lib:${stdenv.cc.cc.lib}/lib' "$out/lib/libk4a.so.${version}"

          echo "Adjusting RPATH for libk4arecord"
          patchelf --set-rpath '${placeholder "out"}/lib/libk4a1.4:${stdenv.cc.cc.lib}/lib' "$out/lib/libk4arecord.so.${version}"

          autoPatchelf "$out/lib/libk4a1.4/libdepthengine.so.2.0"
        '';
      };
    };
in  callPackage ./common.nix ({inherit version;} // builtins.getAttr version versions)
