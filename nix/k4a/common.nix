{ stdenv, dpkg, autoPatchelfHook, udev, libGL, libX11
, version, patches ? [], k4a, k4a-dev, postFixup }:

stdenv.mkDerivation {
  name = "k4a";
  inherit version patches postFixup;

  nativeBuildInputs = [ dpkg autoPatchelfHook ];
  buildInputs = [ udev stdenv.cc.cc.lib libGL ];

  src = builtins.fetchurl {
    inherit (k4a) url sha256;
  };

  devSrc = builtins.fetchurl {
    inherit (k4a-dev) url sha256;
  };

  unpackPhase = ''
    dpkg-deb -x $src ./
    dpkg-deb -x $devSrc ./
  '';

  installPhase = ''
    mkdir -p "$out"
    cp -R "./usr/include" "$out/include"
    cp -R "./usr/lib/x86_64-linux-gnu/" "$out/lib"
    cp -R "./usr/share" "$out/share"
  '';

  dontAutoPatchelf = true;
}
