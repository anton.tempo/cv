{ lib, stdenv, fetchFromGitHub, cmake, alsaLib, libjack2, libpulseaudio }:

stdenv.mkDerivation rec {
  version = "1.1.0";
  pname = "libsoundio";

  src = fetchFromGitHub {
    owner = "andrewrk";
    repo = "libsoundio";
    rev = version;
    sha256 = "0mw197l4bci1cjc2z877gxwsvk8r43dr7qiwci2hwl2cjlcnqr2p";
  };

  nativeBuildInputs = [ cmake ];

  buildInputs = [ libjack2 libpulseaudio ]
    ++ lib.optional stdenv.isLinux alsaLib;

  NIX_CFLAGS_COMPILE = lib.optionalString stdenv.isDarwin "-Wno-strict-prototypes";

  meta = with lib; {
    description = "Cross platform audio input and output";
    homepage = "http://libsound.io/";
    license = licenses.mit;
    platforms = platforms.unix;
    maintainers = [ maintainers.andrewrk ];
  };
}
