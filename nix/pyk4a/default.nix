{ python3, fetchFromGitHub, k4a }:

python3.pkgs.buildPythonPackage {
  pname = "pyk4a";
  version = "1.2.3";

  buildInputs = [ python3 k4a python3.pkgs.numpy python3.pkgs.pytest ];

  postPatch = ''
    echo "POST PATCH!!!!"
    sed -i "/python_version >= \"3.4\"/d" ./setup.cfg
    cat setup.cfg
  '';

  src = fetchFromGitHub {
    owner = "etiennedub";
    repo = "pyk4a";
    rev = "0554de54a65b7fc578fa439c2235140d8fd3d72d";
    sha256 = "sha256:1qsdd4pb5xky752l0ln718wldwc5vkb69hp8740sl8p9s52jfwrr";
  };
}

