let
  k4a = version: self: super: {
    libsoundio-1 = super.callPackage ./libsoundio-1/default.nix {};
    k4a = super.callPackage ./k4a/default.nix { inherit version; };
    k4a-tools = super.callPackage ./k4a-tools/default.nix { inherit version; };
  };

  pyk4a = self: super: {
    pyk4a = super.callPackage ./pyk4a/default.nix {};
  };

  opencv = self: super: {
    opencv4 = super.opencv4.override {
      enableGtk3 = true;
    };
  };
in [ (k4a "1.4.1") pyk4a opencv ]
