import cv2
import numpy as np
import pyk4a
from pyk4a import Config, PyK4A

sift = cv2.SIFT_create()

def prepare_sift():
    img = cv2.imread('object.png')
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = sift.detectAndCompute(img, None)
    img = cv2.drawKeypoints(img, kp, img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imwrite('object_keypoints.png', img)
    return img, kp, des


def camera_loop(obj_img, obj_kp, obj_des):
    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    # search_params = dict(checks=50)   # or pass empty dictionary
    search_params = dict()   # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params,search_params)

    k4a = PyK4A(
        Config(
            color_resolution=pyk4a.ColorResolution.RES_720P,
            depth_mode=pyk4a.DepthMode.NFOV_UNBINNED,
            synchronized_images_only=True,
        )
    )
    k4a.start()
    while 1:
        capture = k4a.get_capture()
        if not np.any(capture.color):
            continue

        color = capture.color[:, :, :3]
        # frame = cv2.cvtColor(color, cv2.COLOR_BGR2GRAY)
        frame = color
        kp2, des2 = sift.detectAndCompute(frame,None)
        # out = np.empty_like(color)
        # out = cv2.drawKeypoints(color, kp2, out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        matches = flann.knnMatch(obj_des,des2,k=2)

        # Need to draw only good matches, so create a mask
        matchesMask = [[0,0] for i in range(len(matches))]

        # ratio test as per Lowe's paper
        for i,(m,n) in enumerate(matches):
            if m.distance < 0.7*n.distance:
                matchesMask[i]=[1,0]

        draw_params = dict(matchColor = (0,255,0),
                        singlePointColor = (255,0,0),
                        matchesMask = matchesMask,
                        flags = cv2.DrawMatchesFlags_DEFAULT)

        match_img = cv2.drawMatchesKnn(obj_img,obj_kp,frame,kp2,matches,None,**draw_params)

        cv2.imshow("k4a", match_img)
        key = cv2.waitKey(10)
        if key == 13:
            cv2.destroyAllWindows()
            break
    k4a.stop()


def main():
    img, kp, des = prepare_sift()
    camera_loop(img, kp, des)


if __name__ == "__main__":
    main()
