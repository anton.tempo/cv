builtins.fetchGit {
  url = "https://github.com/NixOS/nixpkgs.git";
  ref = "master";
  rev = "702cd039c224689d18629689bb6fca0fd2877c19";
}
